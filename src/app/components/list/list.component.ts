import { 
  Component, OnInit 
  , Input
} from '@angular/core';
import { Router } from '@angular/router';
import { MatTableDataSource } from '@angular/material';
// import { } from '@angular/material'; // snoop around...

import { Issue } from 'src/app/models/issue';
import { IssueService } from 'src/app/services/issue.service';

import { AuthService } from 'src/app/services/auth.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  
  @Input('isError') isError;

  issues: Issue [];
  displayedColumns = [ 'title', 'responsible', 'severity', 'status', 'actions' ];
  
  // constructor( private authService: AuthService, private issueService: IssueService, private router: Router ) { } 
  constructor( public authService: AuthService, private issueService: IssueService, private router: Router ) { } // for [ enableProd()\ng build --prod ] purposes

  ngOnInit() {
    this.isError = null;
    this.fetchIssues();
  }

  fetchIssues(){
    this.issueService.getIssues().subscribe( 
      ( data: Issue[] ) => {
        // console.log( "list.component - data" , data );
        this.issues = data;
        // console.log( 'Data requested... this.issues ' , this.issues );
      } 
      , ( err ) => {
        // console.log( "fetchIssues() err" , err );
        this.isError = err;
      }
    );
  }

  editIssue(id){
    this.router.navigate( [ `/edit/${id}` ] );
  }

  deleteIssue(id){
    this.issueService.deleteIssue(id).subscribe( 
      
      () => {
        this.fetchIssues();
      } 
      , ( err ) => {
        // console.log( "deleteIssue(id) err" , err );
        this.isError = err;
      }

    );
  }

}
