import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { BaseurlService } from './baseurl.service';

@Injectable({
  providedIn: 'root'
})
export class EventService {

  // baseUrl:string = "";

  // private eventsUrl: string = ""; 
  private specialEventsUrl: string = "";

  constructor( private http: HttpClient /* , private baseurlservice : BaseurlService */ ){

    // this.baseUrl = this.baseurlservice.baseUrl;
    
    // "http://localhost:3000/api/events";
    // this.eventsUrl = "/api/events";                                  // YES WITH PROXY
    // this.eventsUrl = `${this.baseUrl}/api/events`;                   // YES
    // this.eventsUrl = `/.netlify/functions/getAllEvents`;             // NETLIFY ADDRESS


    // "http://localhost:3000/api/special"; 
    // this.specialEventsUrl = "/api/special";                          // YES WITH PROXY
    // this.specialEventsUrl = `${this.baseUrl}/api/special`;           // YES
    // this.specialEventsUrl = `/.netlify/functions/getAllSpecialevents`;  // NETLIFY ADDRESS
    // this.specialEventsUrl = `/.netlify/functions/getallspecialevents/getallspecialevents`;  // NETLIFY ADDRESS
    this.specialEventsUrl = `/getallspecialevents`;  // NETLIFY ADDRESS

  }

  // getEvents(){
  //   return this.http.get<any>( this.eventsUrl );
  // }

  getSpecialEvents(){
    return this.http.get<any>( this.specialEventsUrl );
  }

}
