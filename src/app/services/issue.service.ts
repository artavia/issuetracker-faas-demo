import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
// import { BaseurlService } from './baseurl.service';

@Injectable({
  providedIn: 'root'
})
export class IssueService {

  // baseUrl:string = "";

  // private issuesUrl: string = ""; 

  constructor( private http: HttpClient /* , private baseurlservice : BaseurlService */ ) { 

    // this.baseUrl = this.baseurlservice.baseUrl;

    // "http://localhost:3000/issues"; 

    // this.issuesUrl = "/issues";                          // YES WITH PROXY
    // this.issuesUrl = `${this.baseUrl}/issues`;              // YES

  }

  getIssues(){
    // return this.http.get( `${this.issuesUrl}` );     // NORMAL VERSION
    // return this.http.get( `/.netlify/functions/getAllIssues` ); // NETLIFY ADDRESS
    // return this.http.get( `/.netlify/functions/getallissues/getallissues` ); // NETLIFY ADDRESS
    return this.http.get( `/getallissues` ); // NETLIFY ADDRESS
  }

  getIssueById(id){
    // return this.http.get( `${this.issuesUrl}/${id}` );     // NORMAL VERSION
    // return this.http.get( `/.netlify/functions/getOneIssue/${id}` );  // NETLIFY ADDRESS
    // return this.http.get( `/.netlify/functions/getoneissue/getoneissue/${id}` );  // NETLIFY ADDRESS
    return this.http.get( `/getoneissue/${id}` );  // NETLIFY ADDRESS
  }

  addIssue( obj ){

    let { title, responsible, description, severity } = obj;
    
    const issue = {
      title: title, responsible: responsible, description: description, severity: severity 
    };
    
    // return this.http.post( `${this.issuesUrl}/add` , issue ); // NORMAL VERSION
    // return this.http.post( `/.netlify/functions/createIssue` , issue ); // NETLIFY ADDRESS
    // return this.http.post( `/.netlify/functions/createissue/createissue` , issue ); // NETLIFY ADDRESS
    return this.http.post( `/createissue` , issue ); // NETLIFY ADDRESS
  }

  updateIssue( obj ){ 
    // console.log( "issue.service >>>>> " , obj );

    // let { id, title, responsible, description, severity, status } = obj;
    // const issue = { title: title, responsible: responsible, description: description, severity: severity, status: status };
    
    // return this.http.post( `${this.issuesUrl}/update/${id}` , issue ); // NORMAL VERSION 
    // return this.http.post( `/.netlify/functions/updateOneIssue` , obj ); // NETLIFY ADDRESS
    // return this.http.post( `/.netlify/functions/updateoneissue/updateoneissue` , obj ); // NETLIFY ADDRESS
    return this.http.post( `/updateoneissue` , obj ); // NETLIFY ADDRESS

  }

  deleteIssue( id ){ 
    // return this.http.get( `${this.issuesUrl}/delete/${id}` ); // NORMAL VERSION
    // return this.http.post( `/.netlify/functions/deleteOneIssue` , id ); // NETLIFY ADDRESS
    // return this.http.post( `/.netlify/functions/deleteoneissue/deleteoneissue` , id ); // NETLIFY ADDRESS
    return this.http.post( `/deleteoneissue` , id ); // NETLIFY ADDRESS
  }

}
