// import querystring from 'querystring';

// =============================================
// process.env SETUP
// =============================================
const { 
  CONNECTION_URL 
} = process.env; 

// BASE SETUP
// =============================================
// import mongoose from 'mongoose'; // original
const mongoose = require('mongoose');

// =============================================
// DB CONNECTION 
// =============================================
mongoose.connect( CONNECTION_URL , {
  useNewUrlParser: true 
  , useUnifiedTopology: true
  , useFindAndModify: false
  , useCreateIndex: true
} );
const db = mongoose.connection;
// db.on( 'open' , console.log.bind( console, `MongoDB database connection established` ) );
// db.once( 'open' , () => {
//   console.log(  `MongoDB database connection established` );
// } );
db.on( 'error' , console.error.bind( console, 'MongoDB connection error, bif!: ' ) );
// =============================================
// END CONNECTION 
// =============================================


// =============================================
// MODEL SCHEMA
// =============================================

// delete mongoose.connection.models['Issue'];

const schema = new mongoose.Schema( {
  
  title: { type: String }
  , responsible: { type: String }
  , description: { type: String } 
  , severity: { type: String } 
  , status: { type: String, default: 'Open' }

  // , completed: { type: Boolean , default: false } 
} );

// const Issue = mongoose.model( 'Issue' , schema );
let Issue = mongoose.connection.models.Issue || mongoose.model('Issue', schema);

// =============================================
// END MODEL SCHEMA
// =============================================

exports.handler = async (event, context) => {

  // accessible through...
  // http://localhost:9000/.netlify/functions/updateOneIssue
  
  // context.callbackWaitsForEmptyEventLoop = false;

  // your server-side functionality 
  // console.log( "context", context ); // { clientContext: {} }
  
  // console.log( "event.path", event.path ); // "Path parameter", e.g. -- /updateOneIssue
  // console.log( "event.httpMethod", event.httpMethod ); //"Incoming request's method name"
  // // console.log( "event.headers", event.headers ); // {Incoming request headers}
  // console.log( "event.queryStringParameters", event.queryStringParameters ); // {query string parameters }
  // console.log( "event.body", event.body ); // "A JSON string of the request payload."
  // console.log( "event.isBase64Encoded", event.isBase64Encoded ); // "A boolean flag to indicate if the applicable request payload is Base64-encode"
  
  if (event.httpMethod !== "POST") { // only POST 
    return {
      statusCode: 405
      , body: JSON.stringify( { errormessage: "Method Not Allowed" } )
    };
  }

  try{
    
    let data = await JSON.parse( event.body );
    let { id , ...issueObject } = await data;

    // console.log( "updateOneIssue NETLEVEE >>>>> await id " , await id );
    // console.log( "updateOneIssue NETLEVEE >>>>> await issueObject " , await issueObject );

    let updatedissue = await Issue.findById( await id );

    updatedissue.title = await issueObject.title;
    updatedissue.responsible = await issueObject.responsible;
    updatedissue.description = await issueObject.description;
    updatedissue.severity = await issueObject.severity;
    updatedissue.status = await issueObject.status;
    
    await updatedissue.save(); 
    
    const netlifyresponseobject = {
      statusCode: 200
      , body: JSON.stringify( { message: "Issue has been updated" } )
    };

    return netlifyresponseobject;

  }
  catch(err){

    console.log( 'update one issue err catch: ', err );
    
    const netlifyresponseerror = {
      statusCode: 500
      , body: JSON.stringify( { errormessage: err.message } )
    };
    
    return netlifyresponseerror;
  }

  
};