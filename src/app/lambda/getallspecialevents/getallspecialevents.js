
// =============================================
// process.env SETUP
// =============================================
const { 
  CONNECTION_URL 
} = process.env; 

// BASE SETUP
// =============================================
// import mongoose from 'mongoose'; // original
const mongoose = require('mongoose');

// =============================================
// DB CONNECTION 
// =============================================
mongoose.connect( CONNECTION_URL , {
  useNewUrlParser: true 
  , useUnifiedTopology: true
  , useFindAndModify: false
  , useCreateIndex: true
} );
const db = mongoose.connection;
// db.on( 'open' , console.log.bind( console, `MongoDB database connection established` ) );
// db.once( 'open' , () => {
//   console.log(  `MongoDB database connection established` );
// } );
db.on( 'error' , console.error.bind( console, 'MongoDB connection error, bif!: ' ) );
// =============================================
// END CONNECTION 
// =============================================

// =============================================
// MODEL SCHEMA
// =============================================

// delete mongoose.connection.models['Specialevent'];

const schema = new mongoose.Schema( {
  
  // _id: mongoose.Schema.Types.ObjectId
  // , 

  name: { type: String }
  , description: { type: String }
  , date: { type: Date }
  
} );

// const Specialevent = mongoose.model( 'Specialevent' , schema );
let Specialevent = mongoose.connection.models.Specialevent || mongoose.model('Specialevent', schema);

// =============================================
// END MODEL SCHEMA
// =============================================

exports.handler = async (event, context) => {

  // accessible through...
  // http://localhost:9000/.netlify/functions/getAllSpecialevents
  
  // context.callbackWaitsForEmptyEventLoop = false;

  // your server-side functionality 
  // console.log( "context", context ); // { clientContext: {} }
  // console.log( "callback", callback );
  // console.log( "event.path", event.path ); // "Path parameter", e.g. -- /getAllSpecialevents
  //// console.log( "event.httpMethod", event.httpMethod ); //"Incoming request's method name"
  // console.log( "event.headers", event.headers ); // {Incoming request headers}
  // console.log( "event.queryStringParameters", event.queryStringParameters ); // {query string parameters }
  //// console.log( "event.body", event.body ); // "A JSON string of the request payload."
  // console.log( "event.isBase64Encoded", event.isBase64Encoded ); // "A boolean flag to indicate if the applicable request payload is Base64-encode"
  
  try{
    
    const specialevents = await Specialevent.find(); // console.log( 'await specialevents: ', await specialevents );

    const netlifyresponseobject = {
      statusCode: 200 
      , body: JSON.stringify( await specialevents ) 
    };

    return netlifyresponseobject;
    
  }
  catch(err){

    console.log( 'specialevents err catch: ', err );
    
    const netlifyresponseerror = {
      statusCode: 500
      , body: JSON.stringify( { errormessage: err.message } )
    };
    
    return netlifyresponseerror;
  }

  //
};