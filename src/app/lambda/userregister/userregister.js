
// =============================================
// process.env SETUP
// =============================================
const { 
  CONNECTION_URL 
} = process.env; 

// BASE SETUP
// =============================================
// import mongoose from 'mongoose'; // original
const mongoose = require('mongoose');

// =============================================
// DB CONNECTION 
// =============================================
mongoose.connect( CONNECTION_URL , {
  useNewUrlParser: true 
  , useUnifiedTopology: true
  , useFindAndModify: false
  , useCreateIndex: true
} );
const db = mongoose.connection;
// db.on( 'open' , console.log.bind( console, `MongoDB database connection established` ) );
// db.once( 'open' , () => {
//   console.log(  `MongoDB database connection established` );
// } );
db.on( 'error' , console.error.bind( console, 'MongoDB connection error, bif!: ' ) );
// =============================================
// END CONNECTION 
// =============================================

// =============================================
// MODEL SCHEMA
// =============================================

// const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');

const SALT_ROUNDS = 10;

// delete mongoose.connection.models['User'];

const schema = new mongoose.Schema( {

  // _id: mongoose.Schema.Types.ObjectId  
  // , 
  
  // username: { type: String }
  // username: { type: String, required: true, unique: true }

  // , email: { type: String }
  // , 
  email: { type: String, required: true, unique: true }
  // , password: { type: String }
  , password: { type: String, required: true }
  
} );

schema.pre( "save" , function(next){
  
  // only has the pdub if new or modified
  if( !this.isModified('password') ){ 
    return next(); 
  }
  if (this.isNew || this.isModified('password')){
    
    let document = this;
    
    // asyncronously generate a salt
    bcrypt.genSalt( SALT_ROUNDS , function( err, salt ){
      
      if( err ){ 
        next(err); // return next(err); 
      } 

      // asyncronously hash the pdub using the new salt
      bcrypt.hash( document.password, salt, function( err, hashedPassword ){
        
        if( err ){ 
          next(err); // return next(err); 
        }

        document.password = hashedPassword;
        next();
      } );

    } );
  }
  else{
    next();
  }

} ); 

schema.methods.comparePdub = function(plntxtpdub, cb ){
  bcrypt.compare( plntxtpdub , this.password, function( err, isMatch ){
    if( err ){ return cb(err); } 
    // cb( err, isMatch ); // generate the generic status text error from here
    cb( null, isMatch );   // generate the specific error text from the route
  } );
}; 

// const User = mongoose.model( 'User' , schema );
let User = mongoose.connection.models.User || mongoose.model('User', schema);

// =============================================
// END MODEL SCHEMA
// =============================================

exports.handler = async (event, context) => {

  // accessible through...
  // http://localhost:9000/.netlify/functions/updateOneUser
  
  // context.callbackWaitsForEmptyEventLoop = false;

  // your server-side functionality 
  // console.log( "context", context ); // { clientContext: {} }
  
  // console.log( "event.path", event.path ); // "Path parameter", e.g. -- /userRegister
  // console.log( "event.httpMethod", event.httpMethod ); //"Incoming request's method name"
  // // console.log( "event.headers", event.headers ); // {Incoming request headers}
  // console.log( "event.queryStringParameters", event.queryStringParameters ); // {query string parameters }
  // console.log( "event.body", event.body ); // "A JSON string of the request payload."
  // console.log( "event.isBase64Encoded", event.isBase64Encoded ); // "A boolean flag to indicate if the applicable request payload is Base64-encode"

  if (event.httpMethod !== "POST") { // only POST
      
    const netlifyresponseerror = {
      statusCode: 405
      , body: JSON.stringify( { errormessage: "Method Not Allowed" } )
    };
    
    return netlifyresponseerror;

  }
  
  try{ 
    
    let userData = await JSON.parse( event.body );

    let user = new User( await userData );

    await user.save();

    const netlifyresponseobject = {
      statusCode: 200
      , body: JSON.stringify( { message: "User has been added" } )
    };

    return netlifyresponseobject;

  }
  catch(err){
    
    console.log( 'user register err catch: ', err );
    
    const netlifyresponseerror = {
      statusCode: 500
      , body: JSON.stringify( { errormessage: err.message } )
    };
    
    return netlifyresponseerror;
  }

  //
};

/* // Docs on event and context https://www.netlify.com/docs/functions/#the-handler-method
exports.handler = async (event, context) => {
  try {
    const subject = event.queryStringParameters.name || 'World'
    return {
      statusCode: 200,
      body: JSON.stringify({ message: `Hello ${subject}` })
      // // more keys you can return:
      // headers: { "headerName": "headerValue", ... },
      // isBase64Encoded: true,
    }
  } catch (err) {
    return { statusCode: 500, body: err.toString() }
  }
}  */